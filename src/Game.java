
import java.util.InputMismatchException;
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author jarew
 */
public class Game {

    Scanner sc = new Scanner(System.in);
    private Table table;
    private int row;
    private int col;
    private Player o;
    private Player x;

    public Game() {
        o = new Player('O');
        x = new Player('X');
    }

    public void startGame() {
        table = new Table(o, x);
    }

    public void showWelcome() {
        System.out.println("----- Welcome to OX GAME -----");
    }

    public void showTable() {
        System.out.println("_ 1 2 3 _");
        for (int i = 0; i < 3; i++) {
            System.out.print((i + 1) + " ");
            for (int j = 0; j < 3; j++) {
                System.out.print(table.getData()[i][j] + " ");
            }
            System.out.print("|");
            System.out.println("");
        }
        System.out.println("¯ ¯ ¯ ¯ ¯");

    }

    public void showTurn() {
        this.table.getCurrentPlayer();
        if (this.table.getCurrentPlayer().getName() == 'X') {
            System.out.println("Turn  Player 2 (X)");
        } else {
            System.out.println("Turn  Player 1 (O)");
        }

    }

    public boolean inputRowCol() {
        System.out.print("Please enter Row and Col :");
        try {
            row = sc.nextInt();
            col = sc.nextInt();

            if (table.getData()[row - 1][col - 1] == '-') {
                table.setRowCol(row, col);
                return true;
            } else {
                System.out.println("Is not empty please try again");
                return false;
            }
        } catch (InputMismatchException e) {
            System.out.println("Input is wrong try input number");
            sc.nextLine();
            return false;
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("row or col is wrong try again");
            return false;
        }
    }

    public boolean inputContinue() {
        while (true) {
            System.out.println("Continue?(y/n)");
            char result = sc.next().charAt(0);
            if (result == 'n') {
                return false;
            } else if (result == 'y') {
                return true;
            }
        }
    }

    public void showBye() {
        System.out.println("----- ! Bye ! -----");
    }

    public void showWin(Player player) {
        if (player.getName() == 'X') {
            this.x.win();
            this.o.lose();
            this.showTable();
            System.out.println("!!! Winer : Player 2 (X) !!!");
        } else if (player.getName() == 'O') {
            this.o.win();
            this.x.lose();
            this.showTable();
            System.out.println("!!! Winer : Player 1 (O) !!!");
        } else {
            this.o.draw();
            this.x.draw();
            this.showTable();
            System.out.println("!!! Draw !!!");
        }
    }

    public void run() {
        this.startGame();
        this.showWelcome();
        while (true) {
            this.runOnce();
            this.showScore();
            if (!this.inputContinue()) {
                this.showBye();
                return;
            } else {
                table.setEmptyTable();
            }
        }
    }

    public void runOnce() {

        while (true) {
            this.showTable();
            this.showTurn();
            if (this.inputRowCol()) {
                if (table.checkWin(row, col)) {
                    this.showWin(table.getWinner());
                    break;
                }
                table.swithTurn();
            }

        }
    }

    public void showScore() {
        System.out.println("Player 1 (O) Win : " + o.getWin() + " Lose : " + o.getLose() + " Draw : " + o.getDraw());
        System.out.println("Player 2 (X) Win : " + x.getWin() + " Lose : " + x.getLose() + " Draw : " + x.getDraw());

    }
}
