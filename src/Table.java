/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author jarew
 */
public class Table {

    private char data[][];
    private Player currentPlayer;
    private Player o;
    private Player x;
    private Player win;
    private int round = 0;

    public Table(Player o, Player x) {
        data = new char[3][3];
        this.o = o;
        this.x = x;
        this.setEmptyTable();
        this.currentPlayer = this.random(o, x);
    }

    public void setEmptyTable() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                this.data[i][j] = '-';
            }
        }
    }

    public void setRowCol(int row, int col) {
        this.data[row - 1][col - 1] = currentPlayer.getName();
        round++;
    }

    public boolean checkWin(int row, int col) {
        if (round == 9) {
            win = new Player('d');
            return true;
        }
        if (checkRow(row) || checkCol(col) || checkX1() || checkX2()) {
            win = currentPlayer;
            return true;
        } else {
            return false;
        }
    }

    public Player getWinner() {
        return this.win;
    }

    public void swithTurn() {
        if (currentPlayer.getName() == 'O') {
            currentPlayer = x;
        } else {
            currentPlayer = o;
        }
    }

    public Player getCurrentPlayer() {
        return currentPlayer;
    }

    public char[][] getData() {
        return data;
    }

    public boolean checkRow(int row) {
        if (data[row - 1][0] == (currentPlayer.getName())) {
            if (data[row - 1][1] == (currentPlayer.getName())) {
                if (data[row - 1][2] == (currentPlayer.getName())) {
                    return true;
                }
            }
        }
        return false;
    }

    public boolean checkCol(int col) {
        if (data[0][col - 1] == (currentPlayer.getName())) {
            if (data[1][col - 1] == (currentPlayer.getName())) {
                if (data[2][col - 1] == (currentPlayer.getName())) {
                    return true;
                }
            }
        }
        return false;
    }

    public boolean checkX1() {
        if (data[2][0] == currentPlayer.getName()) {
            if (data[1][1] == currentPlayer.getName()) {
                if (data[0][2] == currentPlayer.getName()) {
                    return true;
                }
            }
        }
        return false;
    }

    public boolean checkX2() {
        if (data[0][0] == currentPlayer.getName()) {
            if (data[1][1] == currentPlayer.getName()) {
                if (data[2][2] == currentPlayer.getName()) {
                    return true;
                }
            }
        }
        return false;
    }

    public Player random(Player o, Player x) {
        if (this.getRandom(1, 100) % 2 == 1) {
            return o;
        } else {
            return x;
        }
    }

    public int getRandom(int min, int max) {
        int x = (int) ((Math.random() * ((max - min) + 1)) + min);
        return x;
    }
}
