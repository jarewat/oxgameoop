/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 *
 * @author jarew
 */
public class Player {

    private char name;
    private int win;
    private int lose;
    private int draw;

    public Player(char name) {
        this.name = name;
        this.win = 0;
        this.lose = 0;
        this.draw = 0;
    }

    public char getName() {
        return name;
    }

    public void setName(char name) {
        this.name = name;
    }

    public int getWin() {
        return win;
    }

    public int getLose() {
        return lose;
    }

    public int getDraw() {
        return draw;
    }

    public void win() {
        this.win += 1;
    }

    public void lose() {
        this.lose += 1;
    }

    public void draw() {
        this.draw += 1;
    }
}
